'''
MAIN: Transcode video
'''
import os
import json
import logging

from _phase import *
from moviepy.video.io.VideoFileClip import VideoFileClip

HEIGHT = 1080
WIDTH = 1920
PH = Phase()


def transcode(ch, method, properties, body):
    logger.info('Got message: {}'.format(body))
    mdata = json.loads(body.decode('utf-8'))

    filename = mdata['original_file_name']
    id = mdata['id']
    paths = get_paths(id)

    if not os.path.exists(paths['video']):
        return ch.basic_ack(delivery_tag=method.delivery_tag)

    in_path_vid = os.path.join(WORK_DIR, filename)
    tmp_path_vid = os.path.join(WORK_DIR, 'tmp.' + filename)
    os.rename(in_path_vid, tmp_path_vid)

    test_clip = VideoFileClip(tmp_path_vid)
    raw_clip = VideoFileClip(tmp_path_vid,
                             target_resolution=(HEIGHT, int(test_clip.w * (HEIGHT/test_clip.h))),
                             resize_algorithm='bicubic')

    raw_clip.write_videofile(paths['video'], verbose=False, progress_bar=False)
    os.remove(tmp_path_vid)

    PH.send_message(json.dumps(mdata))
    ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == '__main__':
    PH.startup(transcode).start_consuming()
