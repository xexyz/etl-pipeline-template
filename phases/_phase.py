import os
import json
import logging
import sys
import pika

__all__ = ['WORK_DIR', 
           'LOG_FORMAT',
           'Phase',
           'logger',
           'get_paths',
           'get_data',
           'put_data',]

WORK_DIR = os.getenv('WORK_DIR')

LOG_FORMAT = os.getenv('LOG_FORMAT')
fmter = logging.Formatter(LOG_FORMAT)
logger = logging.getLogger()
logger.setLevel(logging.INFO)
l1 = logging.StreamHandler(sys.stdout)
l1.setLevel(logging.INFO)
l1.setFormatter(fmter)
l2 = logging.StreamHandler()
l2.setLevel(logging.ERROR)
l2.setFormatter(fmter)
logger.addHandler(l1)
logger.addHandler(l2)

_WRITE_MQ = os.getenv('WRITE_MQ') 
_READ_MQ = os.getenv('READ_MQ') 
_RMQ_HOST = os.getenv('RMQ_HOST', default='localhost')
_RMQ_USER = os.getenv('RMQ_USER', default='guest')
_RMQ_PASS = os.getenv('RMQ_PASS', default='guest')

VIDEO_EXT = '.mp4'
AUDIO_EXT = '.wav'
DATA_EXT = '.json'

def get_paths(id):
    return { 'video': os.path.join(WORK_DIR, '{}{}'.format(id, VIDEO_EXT)),
             'audio': os.path.join(WORK_DIR, '{}{}'.format(id, AUDIO_EXT)),
             'data': os.path.join(WORK_DIR, '{}{}'.format(id, DATA_EXT)), }

def get_data(path):
    with open(path, 'r') as f:
        return json.loads(f.read())

def put_data(path, mdata):
    with open(path, 'w') as f:
        f.write(json.dumps(mdata))

class Phase(object):
    def __init__(self):
        self.connection = None
        self.write_mq = ''
        self.channel = None

    def startup(self, callback):
        self.write_mq = _WRITE_MQ
        self.setup_channels(callback)

        return self.channel

    def setup_channels(self, callback):
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=_RMQ_HOST,
                heartbeat_interval=0,
                credentials=pika.PlainCredentials(_RMQ_USER, _RMQ_PASS),
            )
        )
        self.channel = self.connection.channel()

        self.channel.queue_declare(queue=_READ_MQ, durable=True)
        if _WRITE_MQ:
            self.channel.queue_declare(queue=_WRITE_MQ, durable=True)

        self.channel.basic_qos(prefetch_count=1)
        self.channel.basic_consume(callback,
                              queue=_READ_MQ)

        return self.channel

    def send_message(self, body):
        if not body:
            return
        self.channel.basic_publish(exchange='',
                                   routing_key=self.write_mq,
                                   body=body,
                                   properties=pika.BasicProperties(
                                       delivery_mode=2,
                                   ))
