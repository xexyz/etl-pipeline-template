#!/usr/bin/env python
'''
INJESTION TYPE: download from youtube and injest
manual/cron phase, injest data into the pipeline
'''
from __future__ import unicode_literals
import argparse
import uuid
import logging
import pika
import glob
import os
import json
import youtube_dl

from _phase import *

PH = Phase()

def yt_injest(ch, method, properties, body):
    logger.info('Got message: {}'.format(body))
    mdata = json.loads(body.decode('utf-8'))
    url = mdata['url']

    if url and ('youtube.com/watch' in url or 'movieclips' in url):
        ydl_opts = {
            'format': 'bestaudio/best',
            'no_warning': True,
        }
        try:
            with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                vals = ydl.extract_info(url)
                filename = ydl.prepare_filename(vals)
        except youtube_dl.utils.DownloadError:
            ch.basic_ack(delivery_tag=method.delivery_tag)
            return


        ofn = os.path.splitext(filename)
        for f in glob.glob('./{}*'.format(ofn[0])):
            id = str(uuid.uuid4())
            paths = get_paths(id)
            nfn = os.path.splitext(f)

            new_fn = '{}{}'.format(id, nfn[1]) 
            mdata['id'] = id
            mdata['original_file_name'] = new_fn

            in_video_path = f
            out_video_path = os.path.join(WORK_DIR, new_fn)
            os.rename(in_video_path, out_video_path)

            put_data(paths['data'], {'original_file_name': filename,
                                     'url': url,
                                     'imdbId': mdata['imdbId'],})


            PH.send_message(json.dumps(mdata))

    ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == '__main__':
    PH.startup(yt_injest).start_consuming()
