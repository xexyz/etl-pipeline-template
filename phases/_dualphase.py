import os
import sys
import logging
import pika

__all__ = ['WORK_DIR',
           'LOG_FORMAT',
           'DualPhase',
           'logger',]

WORK_DIR = os.getenv('WORK_DIR')

LOG_FORMAT = os.getenv('LOG_FORMAT')
fmter = logging.Formatter(LOG_FORMAT)
logger = logging.getLogger()
logger.setLevel(logging.INFO)
l1 = logging.StreamHandler(sys.stdout)
l1.setLevel(logging.INFO)
l1.setFormatter(fmter)
l2 = logging.StreamHandler()
l2.setLevel(logging.ERROR)
l2.setFormatter(fmter)
logger.addHandler(l1)
logger.addHandler(l2)

_WRITE_MQ_TRUE = os.getenv('WRITE_MQ_TRUE') 
_WRITE_MQ_FALSE = os.getenv('WRITE_MQ_FALSE') 
_READ_MQ = os.getenv('READ_MQ') 
_RMQ_HOST = os.getenv('RMQ_HOST', default='localhost')
_RMQ_USER = os.getenv('RMQ_USER', default='guest')
_RMQ_PASS = os.getenv('RMQ_PASS', default='guest')


class DualPhase(object):
    def __init__(self):
        self.connection = None
        self.write_mq = { True: '',
                          False: '',}
        self.channel = None

    def startup(self, callback):
        self.write_mq[True] = _WRITE_MQ_TRUE
        self.write_mq[False] = _WRITE_MQ_FALSE
        self.setup_channels(callback)

        return self.channel

    def setup_channels(self, callback):
        self.connection = pika.BlockingConnection(
            pika.ConnectionParameters(
                host=_RMQ_HOST,
                heartbeat_interval=0,
                credentials=pika.PlainCredentials(_RMQ_USER, _RMQ_PASS),
            )
        )
        self.channel = self.connection.channel()

        self.channel.queue_declare(queue=_READ_MQ, durable=True)
        if _WRITE_MQ_TRUE:
            self.channel.queue_declare(queue=_WRITE_MQ_TRUE, durable=True)
        if _WRITE_MQ_FALSE:
            self.channel.queue_declare(queue=_WRITE_MQ_FALSE, durable=True)

        self.channel.basic_qos(prefetch_count=1)
        self.channel.basic_consume(callback,
                              queue=_READ_MQ)

        return self.channel

    def send_message(self, body, where):
        if not body:
            return
        if not where in self.write_mq:
            return
        self.channel.basic_publish(exchange='',
                                   routing_key=self.write_mq[where],
                                   body=body,
                                   properties=pika.BasicProperties(
                                       delivery_mode=2,
                                       )
                                   )
