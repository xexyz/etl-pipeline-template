'''
Direct Traffic
'''
import os
import json
import logging

from _dualphase import *

SPLIT_VALUE = os.getenv('SPLIT_VALUE')

PH = DualPhase()

def split(ch, method, properties, body):
    logger.info('Got message: {}'.format(body))
    mdata = json.loads(body.decode('utf-8'))

    where = mdata.get(SPLIT_VALUE, False)
    PH.send_message(json.dumps(mdata), where)
    ch.basic_ack(delivery_tag=method.delivery_tag)

    
if __name__ == '__main__':
    PH.startup(split).start_consuming()
