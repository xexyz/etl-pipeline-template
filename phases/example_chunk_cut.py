'''
Take a big video file and chunk it into parts
makes smaller easier to handle files
also formats the video to the proper size
'''
import os
import json
import uuid
import logging

from _phase import *
from moviepy.video.io.VideoFileClip import VideoFileClip

CHUNK_SIZE = 180  # seconds
PH = Phase()


def chunk_process(ch, method, properties, body):
    logger.info('Got message: {}'.format(body))
    mdata = json.loads(body.decode('utf-8'))
    id = mdata['id']
    paths = get_paths(id)

    raw_clip = VideoFileClip(paths['video'])
    data = get_data(paths['data'])

    offset = 0
    while offset < raw_clip.duration:
        if offset + CHUNK_SIZE > raw_clip.duration:
            chunk_next = raw_clip.duration
        else:
            chunk_next = offset + CHUNK_SIZE
        new_id = str(uuid.uuid4())
        mdata['id'] = new_id
        new_clip = raw_clip.subclip(offset, chunk_next)
        new_paths = get_paths(new_id)

        new_clip.write_videofile(new_paths['video'], verbose=False, progress_bar=False)
        put_data(new_paths['data'], data)

        offset += CHUNK_SIZE
        PH.send_message(json.dumps(mdata))

    os.remove(paths['video'])
    os.remove(paths['data'])
    ch.basic_ack(delivery_tag=method.delivery_tag)


if __name__ == '__main__':
    PH.startup(chunk_process).start_consuming()
