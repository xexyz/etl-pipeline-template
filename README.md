* pocketsphinx
Run sudo apt-get install python python-all-dev python-pip build-essential swig git libpulse-dev for Python 2, or sudo apt-get install python3 python3-all-dev python3-pip build-essential swig git libpulse-dev for Python 3.
Run pip install pocketsphinx for Python 2, or pip3 install pocketsphinx for Python 3.

* cv2
sudo apt-get install -y python-qt4

* moviepy
apt install ffmpeg

* rabbitmq
wget https://github.com/rabbitmq/rabbitmq-server/releases/download/rabbitmq_v3_6_12/rabbitmq-server_3.6.12-1_all.deb
rpm
sudo dpkg -i rabbitmq-server_3.6.12-1_all.deb

* supervisord
pip install supervisor
git clone https://github.com/mlazarov/supervisord-monitor.git



