#!/bin/bash

NAME=supervisord
DESC=supervisor
ENV=$HOME/conf/environ
USER=USERNAME
PASS=PASSWORD

case "$1" in
  start)
	echo -n "Starting $DESC: "
	. $ENV
	supervisord -c $ETL_DIR/conf/supervisord.conf
	;;
  stop)
	echo -n "Stopping $DESC: "
	. $ENV
	supervisorctl -u $USER -p $PASS stop all
	kill -9 $(cat $PIDFILE)
	;;
  *)
	N=/etc/init.d/$NAME
	# echo "Usage: $N {start|stop|restart|reload|force-reload}" >&2
	echo "Usage: $N {start|stop|restart|force-reload|status}" >&2
	exit 1
	;;
esac
